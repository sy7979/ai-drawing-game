import urllib.request
import os
import glob
import numpy as np
from tensorflow.keras import layers
from tensorflow import keras
import tensorflow as tf

# Get the Class Name
f = open('./datalist.txt', 'r')
classes = f.readlines()
f.close()

classes = [c.replace('\n', '') for c in classes]


# Download the Dataset
def Download():
    base = 'https://storage.googleapis.com/quickdraw_dataset/full/numpy_bitmap/'
    for c in classes:
        cls_url = c.replace('_', '%20')
        path = base+cls_url+'.npy'
        print(path)
        urllib.request.urlretrieve(path, './data/'+c+'.npy')

Download()