import urllib.request
import os
import glob
import numpy as np
from tensorflow.keras import layers
from tensorflow import keras
import tensorflow as tf

def preprocess(data, max_items_per_class=4000):
    x = np.empty([0, 784])
    data = data[0: max_items_per_class, :]
    x = np.concatenate((x, data), axis=0)
    image_size = 28
    # Reshape and normalize
    x_data = x.reshape(x.shape[0], image_size, image_size, 1).astype('float32')

    x_data /= 255.0

    return x_data
