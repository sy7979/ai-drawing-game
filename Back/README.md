> python 3.7 버전 설치 확인
>
> `python --version`

## 1. 가상환경 만들기

```bash
pip3 install -U pip virtualenv

virtualenv --system-site-packages -p python3 ./venv # 디렉터리로 가상환경
```

> gitbash 말고 terminal에서 실행

## 2. 가상환경 진입

```bash
source ./venv/Scripts/activate
```

## 3. requirements.txt 패키지 설치

```bash
(venv) pip install -r requirements.txt
# tensorflow 2.0, django ..
```

