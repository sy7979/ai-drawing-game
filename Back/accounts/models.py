from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from stories.models import Collection, Answer


class User(AbstractUser):
    name = models.CharField(max_length=30)
    age = models.IntegerField(null=True)
    gender = models.IntegerField(default=0)
    total_play_time = models.FloatField(default=0)
    total_play_cnt = models.IntegerField(default=0)
    one_pick = models.OneToOneField(Collection, on_delete=models.SET_NULL, blank=True, null=True, related_name="user_pick")
    last_story = models.IntegerField(blank=True, null=True)
    last_chapter = models.IntegerField(blank=True, null=True)
    is_author = models.IntegerField(default=0)

class Sort(models.Model):
    name = models.CharField(max_length=30)
    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='images/items/', blank=True)
    wear_image = models.ImageField(upload_to='images/items/', blank=True)
    sort = models.ForeignKey(Sort, on_delete=models.SET_NULL, blank=True, null=True)
    answer = models.OneToOneField(Answer, on_delete=models.SET_NULL, blank=True, null=True)
    def __str__(self):
        return self.name

class Wear(models.Model):
    status = models.BooleanField(default=False)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    item = models.ForeignKey(Item, on_delete=models.CASCADE)
    def __str__(self):
        return 'user: {} item: {} status: {}'.format(self.user.username, self.item.name, self.status)

class Review(models.Model):
    content = models.CharField(max_length=100)
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="write_reviews")
    target_user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="get_reviews")
    def __str__(self):
        return self.content

class PlayTime(models.Model):
    start = models.DateTimeField(auto_now_add=True, blank=True)
    end = models.DateTimeField(null=True, blank=True)
    time = models.FloatField(null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='play_time', blank=True)