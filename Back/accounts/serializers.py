from rest_framework import serializers
from .models import *
from stories.serializers import *


# User
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'password', 'name', 'age', 'gender')


# Collection
class CollectionSerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    answer = AnswerSerializer()
    user_pick = serializers.IntegerField(source='user_pick.id', required=False)
    class Meta:
        model = Collection
        fields = '__all__'

class CustomUserModifySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'name', 'age', 'gender')

# Sort
class SortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sort
        fields = '__all__'

# Item
class ItemSerializer(serializers.ModelSerializer):
    sort = SortSerializer(required=False)
    answer = AnswerSerializer(required=False)
    class Meta:
        model = Item
        fields = '__all__'

class CustomItemSerializer(serializers.ModelSerializer):
    sort = SortSerializer(required=False)
    class Meta:
        model = Item
        fields = ('id', 'name', 'image', 'wear_image', 'sort',)

# Wear
class WearSerializer(serializers.ModelSerializer):
    item = CustomItemSerializer(required=False)
    class Meta:
        model = Wear
        fields = '__all__'

# Review
class CustomUserReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'gender')

class ReviewSerializer(serializers.ModelSerializer):
    user = CustomUserReviewSerializer(required=False)
    target_user = CustomUserReviewSerializer(required=False)
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    class Meta:
        model = Review
        fields = '__all__'

class CustomWriteReviewSerializer(serializers.ModelSerializer):
    target_user = CustomUserReviewSerializer(required=False)
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    class Meta:
        model = Review
        fields = ('id', 'content', 'target_user', 'timestamp')

class CustomGetReviewSerializer(serializers.ModelSerializer):
    user = CustomUserReviewSerializer(required=False)
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    class Meta:
        model = Review
        fields = ('id', 'content', 'user', 'timestamp')

class CustomUserSerializer(serializers.ModelSerializer):
    clear_stories = CustomStorySerializer(many=True)
    clear_chapters = ChapterSerializer(many=True, required=False)
    write_reviews = CustomWriteReviewSerializer(many=True, required=False)
    get_reviews = CustomGetReviewSerializer(many=True, required=False)
    one_pick = CollectionSerializer(required=False)
    class Meta:
        model = User
        fields = ('id', 'username', 'name', 'age', 'gender', 'total_play_time', 'total_play_cnt', 'one_pick', 'last_story', 
                'last_chapter', 'clear_stories', 'clear_chapters', 'write_reviews', 'get_reviews', 'is_staff', 'is_author',)