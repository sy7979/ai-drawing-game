from django.urls import path
from .views import base_views, collection_views, item_views, play_views, review_views, sort_views

app_name = 'accounts'

urlpatterns = [
    # base_views.py
    path('', base_views.users),
    path('<int:user_id>/', base_views.user),
    path('signup/', base_views.signup),
    path('modify/', base_views.modify),
    path('author/', base_views.author),

    # collection_views.py
    path('<int:story_id>/<int:chapter_id>/collection/', collection_views.collections),
    path('collection/<int:collection_id>/', collection_views.collection_detail),
    path('one-pick/<int:collection_id>/', collection_views.one_pick),

    # play_views.py
    path('start-play/', play_views.start_play),
    path('end-play/<int:playtime_id>/', play_views.end_play),
    path('last-play/<int:story_id>/<int:chapter_id>/', play_views.last_play),

    # item_views.py
    path('item/create/<int:answer_id>/<int:sort_id>/', item_views.create_item),
    path('item/read/', item_views.read_item), # 관리자
    path('item/read/user/', item_views.user_item), # 유저
    path('item/update/<int:item_id>/<int:sort_id>/', item_views.update_item),
    path('item/delete/<int:item_id>/', item_views.delete_item),
    path('item/wear/', item_views.wear_item),

    # sort_views.py
    path('sort/create/', sort_views.create_sort),
    path('sort/read/', sort_views.read_sort),
    path('sort/update/<int:sort_id>/', sort_views.update_sort),
    path('sort/delete/<int:sort_id>/', sort_views.delete_sort),

    # review_views.py
    path('review/create/<int:target_user_id>/', review_views.create_review),
    path('review/update/<int:review_id>/', review_views.update_review),
    path('review/delete/<int:review_id>/', review_views.delete_review),
]
