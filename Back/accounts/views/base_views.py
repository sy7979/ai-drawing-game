from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], operation_summary="회원 가입", operation_description=
    """
        - username : 아이디
        - password : 비밀번호
        - name : 이름
        - age : 나이
        - gender : 성별
    """)

@api_view(['POST'])
@permission_classes([AllowAny])
def signup(request):
    serializer = UserSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        password = serializer.validated_data.get('password')
        user = serializer.save()
        user.set_password(raw_password=password)
        user.is_active = True
        user.save()
        return JsonResponse({
            "message": "회원 가입 성공"
        })
    return HttpResponse(status=400)


@swagger_auto_schema(methods=['get'], operation_summary="모든 회원 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def users(request):
    users = User.objects.all()
    serializer = CustomUserSerializer(users, many=True)
    return JsonResponse({
        "message": "모든 회원 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['get'], operation_summary="회원 디테일 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def user(request, user_id):
    user = get_object_or_404(User, id=user_id)
    serializer = CustomUserSerializer(user)
    return JsonResponse({
        "message": "회원 디테일 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['put'], operation_summary="회원 정보 수정", request_body=CustomUserModifySerializer, operation_description=
    """
        - name : 이름
        - age : 나이
        - gender : 성별
    """)
@swagger_auto_schema(methods=['delete'], operation_summary="회원 탈퇴")
@api_view(['PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def modify(request):
    user = request.user
    if request.method == 'PUT':
        serializer = CustomUserModifySerializer(data=request.data, instance=user)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({
                "message": "회원 정보 수정 성공",
                "data": serializer.data
            })
        return HttpResponse(status=400)

    elif request.method == 'DELETE':
        user.delete()
        return JsonResponse({
            "message": "회원 탈퇴 성공"
        })
        

@swagger_auto_schema(methods=['get'], operation_summary="모든 작가 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def author(request):
    authors = User.objects.filter(is_author=1)
    serializer = CustomUserSerializer(authors, many=True)
    return JsonResponse({
        "message": "모든 작가 조회 성공",
        "data": serializer.data
    })