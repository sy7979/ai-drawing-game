from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from stories.models import Collection
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['get'], tags=['컬렉션'], operation_summary="모든 컬렉션 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def collections(request, story_id, chapter_id):
    if Collection.objects.count() != 0:
        collections = Collection.objects.filter(user_id=request.user.id, answer__bgimage__chapter__story_id=story_id, answer__bgimage__chapter_id=chapter_id)
        serializer = CollectionSerializer(collections, many=True)
        return JsonResponse({
            "message": "모든 컬렉션 조회 성공",
            "data": serializer.data
        }, safe=False)
    else:
        return JsonResponse({
            "message": "컬렉션이 존재하지 않음"
        }, safe=False)


@swagger_auto_schema(methods=['get'], tags=['컬렉션'], operation_summary="컬렉션 디테일 조회")
@swagger_auto_schema(methods=['delete'], tags=['컬렉션'], operation_summary="컬렉션 삭제")
@api_view(['GET', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def collection_detail(request, collection_id):
    collection = get_object_or_404(Collection, id=collection_id)
    serializer = CollectionSerializer(collection)
    if request.method == 'GET':
        return JsonResponse({
            "message": "특정 컬렉션 조회 성공",
            "data": serializer.data
        }, safe=False)

    elif request.method == 'DELETE':
        collection.delete()
        return JsonResponse({
            "message": "컬렉션 삭제 성공"
        })


@swagger_auto_schema(methods=['get'], tags=['컬렉션'], operation_summary="원픽 설정")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def one_pick(request, collection_id):
    collection = get_object_or_404(Collection, id=collection_id)
    user = request.user
    user.one_pick = collection
    user.save()
    return JsonResponse({
        "message": "원픽 설정 성공"
    }, safe=False)