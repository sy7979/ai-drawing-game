from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['아이템'], operation_summary="아이템 생성")
@api_view(['POST'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def create_item(request, answer_id, sort_id):
    answer = get_object_or_404(Answer, id=answer_id)
    sort = get_object_or_404(Sort, id=sort_id)
    serializer = ItemSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save(answer=answer, sort=sort)
        return JsonResponse({
            "message": "아이템 생성 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['get'], tags=['아이템'], operation_summary="관리자 아이템 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def read_item(request):
    items = Item.objects.all()
    serializer = ItemSerializer(items, many=True)
    return JsonResponse({
        "message": "관리자 아이템 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['get'], tags=['아이템'], operation_summary="유저 아이템 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def user_item(request):
    user = request.user
    items = Wear.objects.filter(user_id=user.id)
    serializer = WearSerializer(items, many=True)
    return JsonResponse({
        "message": "유저 아이템 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['put'], tags=['아이템'], operation_summary="아이템 수정")
@api_view(['PUT'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def update_item(request, item_id, sort_id):
    item = get_object_or_404(Item, id=item_id)
    sort = get_object_or_404(Sort, id=sort_id)
    serializer = ItemSerializer(data=request.data, instance=item)
    if serializer.is_valid(raise_exception=True):
        serializer.save(sort=sort)
        return JsonResponse({
            "message": "아이템 수정 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['delete'], tags=['아이템'], operation_summary="아이템 삭제")
@api_view(['DELETE'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def delete_item(request, item_id):
    item = get_object_or_404(Item, id=item_id)
    item.delete()
    return JsonResponse({
        "message": "아이템 삭제 성공"
    })


@swagger_auto_schema(methods=['post'], tags=['아이템'], operation_summary="아이템 착용")
@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def wear_item(request):
    user = request.user
    items = request.data.get('items')
    items = items.replace('[', '').replace(']', '').split(',')

    wears = Wear.objects.filter(user=user)
    for wear in wears:
        wear.status = False
        wear.save()

    for wear_id in items:
        if wear_id:
            wear = get_object_or_404(Wear, id=int(wear_id))
            wear.status = True
            wear.save()
    
    data = list(user.wear_set.all().values('id', 'item__name', 'status'))
    return JsonResponse({
        "message": "아이템 착용 성공",
        "data": data
    })