from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from datetime import datetime, timezone
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['get'], tags=['플레이'], operation_summary="게임 시작")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def start_play(request):
    user = request.user
    playtime = PlayTime.objects.create(user=user)
    user.total_play_cnt += 1
    user.save()
    return JsonResponse({
        "message": "게임 시작",
        "data": playtime.id
    }, safe=False)


@swagger_auto_schema(methods=['get'], tags=['플레이'], operation_summary="게임 종료")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def end_play(request, playtime_id):
    playtime = get_object_or_404(PlayTime, id=playtime_id)
    playtime.end = datetime.now()
    playtime.time = (datetime.now(timezone.utc) - playtime.start).seconds / 3600 # datetime 타입 같게
    playtime.save()
    user = request.user
    user.total_play_time += playtime.time
    user.save()
    return JsonResponse({
        "message": "게임 종료"
    })


@swagger_auto_schema(methods=['get'], tags=['플레이'], operation_summary="최근 플레이 저장")
@api_view(['GET'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def last_play(request, story_id, chapter_id):
    user = request.user
    user.last_story = story_id
    user.last_chapter = chapter_id
    user.save()
    return JsonResponse({
        "message": "최근 플레이 저장 성공"
    })