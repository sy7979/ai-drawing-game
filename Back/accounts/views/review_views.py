from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['리뷰'], operation_summary="리뷰 생성")
@api_view(['POST'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def create_review(request, target_user_id):
    user = request.user
    target_user = get_object_or_404(User, id=target_user_id)
    serializer = ReviewSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save(user=user, target_user=target_user)
        return JsonResponse({
            "message": "리뷰 생성 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['put'], tags=['리뷰'], operation_summary="리뷰 수정")
@api_view(['PUT'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def update_review(request, review_id):
    review = get_object_or_404(Review, id=review_id)
    serializer = ReviewSerializer(data=request.data, instance=review)
    if serializer.is_valid():
        return JsonResponse({
            "message": "리뷰 수정 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['delete'], tags=['리뷰'], operation_summary="리뷰 삭제")
@api_view(['DELETE'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def delete_review(request, review_id):
    review = get_object_or_404(Review, id=review_id)
    review.delete()
    return JsonResponse({
        "message": "리뷰 삭제 성공"
    })
