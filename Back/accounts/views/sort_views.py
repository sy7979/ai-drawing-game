from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['분류'], operation_summary="분류 생성")
@api_view(['POST'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def create_sort(request):
    serializer = SortSerializer(data=request.data)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse({
            "message": "분류 생성 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['get'], tags=['분류'], operation_summary="모든 분류 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def read_sort(request):
    sorts = Sort.objects.all()
    serializer = SortSerializer(sorts, many=True)
    return JsonResponse({
        "message": "모든 분류 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['put'], tags=['분류'], operation_summary="분류 수정")
@api_view(['PUT'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def update_sort(request, sort_id):
    sort = get_object_or_404(Sort, id=sort_id)
    serializer = SortSerializer(data=request.data, instance=sort)
    if serializer.is_valid():
        serializer.save()
        return JsonResponse({
            "message": "분류 수정 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['delete'], tags=['분류'], operation_summary="분류 삭제")
@api_view(['DELETE'])
@permission_classes([IsAuthenticated]) 
@authentication_classes([JSONWebTokenAuthentication])
def delete_sort(request, sort_id):
    sort = get_object_or_404(Sort, id=sort_id)
    sort.delete()
    return JsonResponse({
        "message": "분류 삭제 성공"
    })
