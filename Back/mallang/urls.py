"""mallang URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from django.conf import settings
from django.conf.urls.static import static
from rest_framework_jwt.views import obtain_jwt_token

# drf_yasg
schema_view = get_schema_view(
   openapi.Info(
      title="그려줘친구들 API",
      default_version='v1',
   ),
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('stories.urls')),
    path('api-token-auth/', obtain_jwt_token),
    path('accounts/', include('accounts.urls')),
    path('swagger/', schema_view.with_ui('swagger')),
    path('docs/', schema_view.with_ui('redoc')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)