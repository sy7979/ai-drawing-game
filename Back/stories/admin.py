from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Story)
admin.site.register(Chapter)
admin.site.register(Answer)
admin.site.register(BackgroundImage)
admin.site.register(Script)
admin.site.register(Collection)
admin.site.register(Application)