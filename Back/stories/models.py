from django.db import models
from django.conf import settings


class Story(models.Model):
    title = models.CharField(max_length=100)
    image = models.ImageField(upload_to='images/stories/')
    difficulty = models.IntegerField()
    color = models.CharField(max_length=30)
    clear_users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='clear_stories', blank=True)
    def __str__(self):
        return self.title

class Chapter(models.Model):
    number = models.IntegerField()
    title = models.CharField(max_length=100)
    story = models.ForeignKey(Story, on_delete=models.CASCADE)
    clear_users = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='clear_chapters', blank=True)
    bgimg = models.IntegerField(blank=True, null=True)
    mission = models.CharField(max_length=100, blank=True, null=True)
    record = models.FileField(upload_to='record/mission/', blank=True, null=True)
    bgm = models.FileField(upload_to='record/bgm/', blank=True, null=True)
    def __str__(self):
        return self.title

class BackgroundImage(models.Model):
    number = models.IntegerField()
    image = models.ImageField(upload_to='images/bgImages/', blank=True)
    chapter = models.ForeignKey(Chapter, on_delete=models.CASCADE)
    def __str__(self):
        return 'chapter: {} number: {}'.format(self.chapter.title, self.number)

class Answer(models.Model):
    en_name = models.CharField(max_length=50)
    ko_name = models.CharField(max_length=50)
    image = models.ImageField(upload_to='images/answers/')
    bgimage = models.ForeignKey(BackgroundImage, on_delete=models.CASCADE, blank=True, null=True)
    def __str__(self):
        return self.ko_name

class Script(models.Model):
    name = models.CharField(max_length=30, default='narration')
    content = models.CharField(max_length=100)
    record = models.FileField(upload_to='record/scripts/')
    background_image = models.ForeignKey(BackgroundImage, on_delete=models.CASCADE)
    def __str__(self):
        return 'chapter: {} bgimg: {} name: {}'.format(self.background_image.chapter.title, self.background_image.number, self.name)

class Collection(models.Model):
    image = models.ImageField(upload_to='images/collections/')
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    def __str__(self):
        return '{}의 정답 그림'.format(self.answer.ko_name)

class Application(models.Model):
    status = models.CharField(max_length=10, default="unread")
    title = models.CharField(max_length=50)
    description = models.TextField()
    files = models.FileField(upload_to='files/applications/')
    phone = models.CharField(max_length=12)
    email = models.CharField(max_length=50)
    timestamp = models.DateTimeField(auto_now_add=True, blank=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)