from rest_framework import serializers
from .models import *
from accounts.models import Sort, Item, User


# Story
class StorySerializer(serializers.ModelSerializer):
    chapter_cnt = serializers.IntegerField(source='chapter_set.count', required=False, read_only=True)
    image = serializers.FileField()
    class Meta:
        model = Story
        fields = ('id', 'title', 'image', 'difficulty', 'color', 'clear_users', 'chapter_cnt',)
        read_only_fields = ('id', 'clear_users',)

class CustomStorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Story
        fields = ('id', 'title', 'color',)

# Chapter
class ChapterSerializer(serializers.ModelSerializer):
    story_id = serializers.IntegerField(source='story.id', required=False, read_only=True)
    record = serializers.FileField(required=False)
    bgm = serializers.FileField(required=False)
    class Meta:
        model = Chapter
        fields = ('id', 'number', 'title', 'story_id', 'bgimg', 'mission', 'record', 'clear_users', 'bgm')
        read_only_fields = ('bgimg', 'clear_users',)

class ChapterMissionSerializer(serializers.ModelSerializer):
    record = serializers.FileField(required=False)
    class Meta:
        model = Chapter
        fields = ('bgimg', 'mission', 'record',)

# Sort
class SortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sort
        fields = '__all__'

# Item
class ItemSerializer(serializers.ModelSerializer):
    sort = SortSerializer(required=False)
    class Meta:
        model = Item
        fields = '__all__'

# Answer
class AnswerSerializer(serializers.ModelSerializer):
    item = ItemSerializer(required=False)
    chapter = ChapterSerializer(source='bgimage.chapter', required=False)
    class Meta:
        model = Answer
        fields = ('id', 'en_name', 'ko_name', 'image', 'bgimage', 'item', 'chapter',)

# Script
class ScriptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Script
        fields = ('id', 'name', 'content', 'record',)

# BackgroundImage
class BackgroundImageSerializer(serializers.ModelSerializer):
    scripts = ScriptSerializer(source='script_set', many=True, required=False, read_only=True)
    answers = AnswerSerializer(source='answer_set', many=True, required=False, read_only=True)
    class Meta:
        model = BackgroundImage
        fields = ('id', 'number', 'image', 'scripts', 'answers',)

class BackgroundImageModifySerializer(serializers.ModelSerializer):
    scripts = ScriptSerializer(source='script_set', many=True, required=False)
    number = serializers.IntegerField(required=False)
    class Meta:
        model = BackgroundImage
        fields = ('id', 'number', 'image', 'scripts',)

# User
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('username', 'name', 'age', 'gender', 'is_author',)

# Application
class ApplicationSerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    user = UserSerializer(required=False)
    class Meta:
        model = Application
        fields = ('id', 'status', 'title', 'description', 'files', 'phone', 'email', 'timestamp', 'user')

class UserApplicationSerializer(serializers.ModelSerializer):
    timestamp = serializers.DateTimeField(format="%Y.%m.%d %H:%M", required=False)
    class Meta:
        model = Application
        fields = ('id', 'status', 'title', 'description', 'files', 'phone', 'email', 'timestamp',)