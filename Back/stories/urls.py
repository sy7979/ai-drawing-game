from django.urls import path
from .views import story_views, chapter_views, bgImage_views, script_views, answer_views, application_views

app_name = 'stories'

urlpatterns = [
    # story_views.py
    path('stories/', story_views.stories),
    path('stories/<int:story_id>/', story_views.modify_stories),

    # chapter_views.py
    path('chapters/<int:id>/', chapter_views.chapters),
    path('chapters/play/<int:story_id>/', chapter_views.game_chapters),
    path('chaptersMission/', chapter_views.chaptersMission),
    path('chapters/clear/<int:chapter_id>/<int:answer_id>/', chapter_views.clear_chapters),

    # bgImage_views.py
    path('bgImages/<int:id>/', bgImage_views.bgImages),
    path('bgImages-detail/<int:bgImage_id>/', bgImage_views.bgImagesDetailModify),

    # script_views.py
    path('scripts/<int:id>/', script_views.scripts),

    # answer_views.py
    path('answers/<int:id>/', answer_views.answers),
    path('answers/<int:answer_id>/check/', answer_views.check_answer),

    # application_views.py
    path('application/create/', application_views.create_application),
    path('application/read/', application_views.read_application), # 관리자
    path('application/read/user/', application_views.user_application), # 유저
    path('application/read/<int:application_id>/', application_views.read_application_detail),
    path('application/delete/<int:application_id>/', application_views.delete_application),
    path('application/response/<int:application_id>/<int:status_id>/', application_views.response_application),

]