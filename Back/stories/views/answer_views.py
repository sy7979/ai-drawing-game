from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema
from AI.predict import predict


@swagger_auto_schema(methods=['post'], tags=['미션'], operation_summary="미션 생성", request_body=AnswerSerializer, operation_description="""/answers/{bgImage_id}/""")
@swagger_auto_schema(methods=['get'], tags=['미션'], operation_summary="모든 미션 조회", operation_description="""/answers/{story_id}/""")
@swagger_auto_schema(methods=['put'], tags=['미션'], operation_summary="미션 수정", request_body=AnswerSerializer, operation_description="""/answers/{answer_id}/""")
@swagger_auto_schema(methods=['delete'], tags=['미션'], operation_summary="미션 삭제", operation_description="""/answers/{answer_id}/""")
@api_view(['POST', 'GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def answers(request, id):
    message = ""
    if request.method == 'POST':
        bgImage = get_object_or_404(BackgroundImage, id=id)
        serializer = AnswerSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(bgimage=bgImage)
            message = "미션 생성 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'GET':
        answers = Answer.objects.filter(bgimage__chapter__story_id=id).order_by('bgimage__chapter__number')
        serializer = AnswerSerializer(answers, many=True)
        message = "모든 미션 조회 성공"

    elif request.method == 'PUT':
        answer = get_object_or_404(Answer, id=id)
        serializer = AnswerSerializer(data=request.data, instance=answer)
        if serializer.is_valid():
            serializer.save()
            message = "미션 수정 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'DELETE':
        answer = get_object_or_404(Answer, id=id)
        answer.delete()
        return JsonResponse({
            "message": "미션 삭제 성공"
            })

    return JsonResponse({
        "message": message,
        "data": serializer.data
        }, safe=False)

import time

@swagger_auto_schema(methods=['post'], tags=['미션'], operation_summary="정답 확인")
@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def check_answer(request, answer_id):
    # get data
    imgData = request.data.get('imgData')
    image = request.FILES.get('image')
    user = request.user
    answer = get_object_or_404(Answer, id=answer_id)

    # model predict
    values = predict(imgData)
    
    # collection save
    if answer.en_name in values.keys():
        Collection.objects.create(user=user, answer=answer, image=image)
        message = "success"
    else:
        message = "fail"

    # 시연용
    # time.sleep(2)
    # Collection.objects.create(user=user, answer=answer, image=image)
    # message = "success"

    return JsonResponse({
        "message": message,
        "data": values
    })
    