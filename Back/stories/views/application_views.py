from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from accounts.models import Wear
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['신청서'], operation_summary="신청서 제출", request_body=ApplicationSerializer)
@api_view(['POST'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def create_application(request):
    serializer = ApplicationSerializer(data=request.data)
    if serializer.is_valid(raise_exception=True):
        serializer.save(user=request.user)
        return JsonResponse({
            "message": "신청서 제출 성공",
            "data": serializer.data
        })
    else: return HttpResponse(status=400)


@swagger_auto_schema(methods=['get'], tags=['신청서'], operation_summary="관리자 신청서 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def read_application(request):
    applications = Application.objects.all()
    serializer = ApplicationSerializer(applications, many=True)
    return JsonResponse({
        "message": "관리자 신청서 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['get'], tags=['신청서'], operation_summary="유저 신청서 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def user_application(request):
    applications = Application.objects.filter(user=request.user)
    serializer = UserApplicationSerializer(applications, many=True)
    return JsonResponse({
        "message": "유저 신청서 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['get'], tags=['신청서'], operation_summary="신청서 디테일 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def read_application_detail(request, application_id):
    application = get_object_or_404(Application, id=application_id)
    serializer = ApplicationSerializer(application)
    return JsonResponse({
        "message": "신청서 디테일 조회 성공",
        "data": serializer.data
    })


@swagger_auto_schema(methods=['delete'], tags=['신청서'], operation_summary="신청서 삭제")
@api_view(['DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def delete_application(request, application_id):
    application = get_object_or_404(Application, id=application_id)
    application.delete()
    return JsonResponse({
        "message": "신청서 삭제 성공"
    })


@swagger_auto_schema(methods=['get'], tags=['신청서'], operation_summary="관리자 신청서 응답")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def response_application(request, application_id, status_id):
    message = ""
    application = get_object_or_404(Application, id=application_id)
    if status_id == 1:
        application.status = "approve"
        user = application.user
        user.is_author = 1
        user.save()
        message = "신청서 승인 성공"
    elif status_id == 0:
        application.status = "disapprove"
        message = "신청서 거절 성공"
    application.save()
    serializer = ApplicationSerializer(application)
    return JsonResponse({
        "message": message,
        "data": serializer.data
    })