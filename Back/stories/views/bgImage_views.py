from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['배경이미지'], operation_summary="배경이미지 생성", request_body=BackgroundImageSerializer, operation_description="""/bgImages/{chapter_id}/""")
@swagger_auto_schema(methods=['get'], tags=['배경이미지'], operation_summary="배경이미지 조회", operation_description=
    """/bgImages/{chapter_id}/
        - number : 순서번호
        - image : 제목
        - scripts : 해당 스크립트정보
        - answers : 해당 정답정보
        - record : 녹음파일

        - "mission_data" : 챕터의 미션정보
        - "answer_data" : 챕터의 모든 정답정보
    """)
@swagger_auto_schema(methods=['put'], tags=['배경이미지'], operation_summary="배경이미지파일 수정", request_body=BackgroundImageModifySerializer, operation_description="""/bgImages/{bgImage_id}}/""")
@swagger_auto_schema(methods=['delete'], tags=['배경이미지'], operation_summary="배경이미지 삭제", operation_description="""/bgImages/{bgImage_id}/""")
@api_view(['POST', 'GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def bgImages(request, id):
    message = ""
    if request.method == 'POST':
        chapter = get_object_or_404(Chapter, id=id)
        serializer = BackgroundImageSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            bgImage = serializer.save(chapter=chapter)
            message = "배경이미지 생성 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'GET':
        chapter = get_object_or_404(Chapter, id=id)
        bgImages = BackgroundImage.objects.filter(chapter_id=id).order_by('number')
        answers = Answer.objects.filter(bgimage__chapter_id=id)
        serializer = BackgroundImageSerializer(bgImages, many=True)
        serializer2 = ChapterMissionSerializer(chapter)
        serializer3 = AnswerSerializer(answers, many=True)
        mission_number = None
        mission = chapter.bgimg
        if mission != None:
            bgImage = get_object_or_404(BackgroundImage, id=mission)
            mission_number = bgImage.number
        return JsonResponse({
            "message": "배경이미지 조회 성공",
            "data": serializer.data,
            "mission_data": serializer2.data,
            "answer_data": serializer3.data,
            "mission_number": mission_number
        }, safe=False)

    elif request.method == 'PUT':
        bgImage = get_object_or_404(BackgroundImage, id=id)
        serializer = BackgroundImageModifySerializer(data=request.data, instance=bgImage)
        if serializer.is_valid():
            bgImage = serializer.save()
            message = "배경이미지파일 수정 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'DELETE':
        bgImage = get_object_or_404(BackgroundImage, id=id)
        chapter_id = bgImage.chapter.id
        chapter = get_object_or_404(Chapter, id=chapter_id)
        chapter.bgimg = None
        chapter.save()
        bgImage.delete()
        return JsonResponse({
            "message": "배경이미지 삭제 성공"
        })

    return JsonResponse({
        "message": message,
        "data": serializer.data
    }, safe=False)


@swagger_auto_schema(methods=['put'], tags=['배경이미지'], operation_summary="배경이미지파일 외 정보 수정", request_body=BackgroundImageSerializer)
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def bgImagesDetailModify(request, bgImage_id):
    bgImage = get_object_or_404(BackgroundImage, id=bgImage_id)
    serializer = BackgroundImageSerializer(data=request.data, instance=bgImage)
    if serializer.is_valid():
        bgImage = serializer.save()
        return JsonResponse({
            "message": "배경이미지 외 디테일 수정 성공",
            "data": serializer.data
        }, safe=False)
    return HttpResponse(status=400)
