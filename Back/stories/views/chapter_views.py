from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from accounts.models import Wear
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['챕터'], operation_summary="챕터 생성", request_body=ChapterSerializer, operation_description=
    """
        /chapters/{story_id}/

        - number : 순서번호
        - title : 제목
        - mission : 미션 내용
        - record : 녹음파일 (테스트 안됨)
    """)
@swagger_auto_schema(methods=['get'], tags=['챕터'], operation_summary="모든 챕터 조회", operation_description=
    """
        /chapters/{story_id}/

        - number : 순서번호
        - title : 제목
        - bgimg : 미션 배경이미지 id
        - mission : 미션 내용
        - record : 녹음파일
    """)
@swagger_auto_schema(methods=['put'], tags=['챕터'], operation_summary="챕터 수정", request_body=ChapterSerializer, operation_description=
    """
        /chapters/{chapter_id}}/

        - number : 순서번호
        - title : 제목
        - mission : 미션 내용
        - record : 녹음파일 (테스트 안됨)
    """)
@swagger_auto_schema(methods=['delete'],tags=['챕터'],  operation_summary="챕터 삭제", operation_description="""/chapters/{chapter_id}/""")
@api_view(['POST', 'GET', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def chapters(request, id):
    message = ""
    if request.method == 'POST':
        story = get_object_or_404(Story, id=id)
        serializer = ChapterSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(story=story)
            message = "챕터 생성 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'GET':
        chapters = Chapter.objects.filter(story_id=id).order_by('number')
        serializer = ChapterSerializer(chapters, many=True)
        message = "모든 챕터 조회 성공"

    elif request.method == 'PUT':
        chapter = get_object_or_404(Chapter, id=id)
        serializer = ChapterSerializer(data=request.data, instance=chapter)
        if serializer.is_valid():
            serializer.save()
            message = "챕터 수정 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'DELETE':
        chapter = get_object_or_404(Chapter, id=id)
        chapter.delete()
        return JsonResponse({
            "message": "챕터 삭제 성공"
        })

    return JsonResponse({
        "message": message,
        "data": serializer.data
    }, safe=False)


@swagger_auto_schema(methods=['get'], tags=['챕터'], operation_summary="게임 챕터 조회")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def game_chapters(request, story_id):
    chapters = Chapter.objects.filter(story_id=story_id).order_by('number')

    chapter_bgimgs = []
    chapter_answers = []
    for chapter in chapters:
        bgimgs = BackgroundImage.objects.filter(chapter_id=chapter.id)
        if len(bgimgs) != 0:
            bgimgs = bgimgs.order_by('number')
            if Answer.objects.count() != 0:
                answers = Answer.objects.filter(bgimage__chapter_id=chapter.id)
                if (len(answers) != 0):
                    chapter_answers.append(True)
                else:
                    chapter_answers.append(False)
            else:
                chapter_answers.append(False)
            serializer2 = BackgroundImageSerializer(bgimgs[0])
            chapter_bgimgs.append(serializer2.data)
        else:
            chapter_bgimgs.append(None)
            chapter_answers.append(False)

    serializer = ChapterSerializer(chapters, many=True)
    return JsonResponse({
        "message": "게임 챕터 조회 성공",
        "data": serializer.data,
        "chapter_Thumbnail": chapter_bgimgs,
        "chapter_Answers": chapter_answers,
    }, safe=False)


@swagger_auto_schema(methods=['put'], tags=['챕터'], operation_summary="각 챕터 미션위치 변경")
@api_view(['PUT'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def chaptersMission(request):
    id = request.data.get("bgImgId")
    bgImage = get_object_or_404(BackgroundImage, id=id)
    chapter_id = bgImage.chapter.id
    chapter = get_object_or_404(Chapter, id=chapter_id)
    chapter.bgimg = id
    chapter.save()
    return JsonResponse({
        "message": "각 챕터 미션위치 변경 성공"
    }, safe=False)


@swagger_auto_schema(methods=['get'], tags=['챕터'], operation_summary="챕터 클리어")
@api_view(['GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def clear_chapters(request, chapter_id, answer_id):
    chapter = get_object_or_404(Chapter, id=chapter_id)
    user = request.user
    
    clear_data = "이미 클리어한 챕터"
    item_data = "이미 획득한 아이템"
    # clear chapters
    if user not in chapter.clear_users.all():
        chapter.clear_users.add(user)
        clear_data = list(user.clear_chapters.all().values('id', 'title'))

    # item save
    item = get_object_or_404(Item, answer_id=answer_id)
    wear = Wear.objects.filter(user=user, item=item)
    if not wear:
        Wear.objects.create(user=user, item=item)
        item_data = ItemSerializer(item).data

    return JsonResponse({
        "message": "챕터 클리어 성공",
        "clear_chapters": clear_data,
        "item_data": item_data,
    })
