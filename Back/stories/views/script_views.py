from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['스크립트'], operation_summary="스크립트 생성", request_body=ScriptSerializer, operation_description=
    """
        /scripts/{bgImage_id}/

        - name : 이름 (default='narration')
        - content : 대사
    """)
@swagger_auto_schema(methods=['put'], tags=['스크립트'], operation_summary="스크립트 수정", request_body=ScriptSerializer, operation_description=
    """
        /scripts/{script_id}}/
    
        - name : 이름
        - content : 대사
    """)
@swagger_auto_schema(methods=['delete'], tags=['스크립트'], operation_summary="스크립트 삭제", operation_description="""/scripts/{script_id}/""")
@api_view(['POST', 'PUT', 'DELETE'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def scripts(request, id):
    message = ""
    if request.method == 'POST':
        bgImage = get_object_or_404(BackgroundImage, id=id)
        serializer = ScriptSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            serializer.save(background_image=bgImage)
            message = "스크립트 생성 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'PUT':
        script = get_object_or_404(Script, id=id)
        serializer = ScriptSerializer(data=request.data, instance=script)
        if serializer.is_valid():
            serializer.save()
            message = "스크립트 수정 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'DELETE':
        script = get_object_or_404(Script, id=id)
        script.delete()
        return JsonResponse({
            "message": "스크립트 삭제 성공"
        })

    return JsonResponse({
        "message": message,
        "data": serializer.data
    }, safe=False)