from django.shortcuts import get_object_or_404
from django.http import JsonResponse, HttpResponse
from rest_framework.decorators import api_view, permission_classes, authentication_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from ..models import *
from ..serializers import *
from drf_yasg.utils import swagger_auto_schema


@swagger_auto_schema(methods=['post'], tags=['스토리'], operation_summary="스토리 생성", request_body=StorySerializer, operation_description=
    """
        - title : 이름
        - image : 대표이미지 (테스트 안됨)
        - difficulty : 난이도
        - color : 색상
    """)
@swagger_auto_schema(methods=['get'], tags=['스토리'], operation_summary="모든 스토리 조회", operation_description=
    """
        - title : 이름
        - image : 대표이미지 (테스트 안됨)
        - difficulty : 난이도
        - color : 색상
        - clear_users : 클리어한 유저들 id
        - chapter_cnt : 챕터 개수
    """)
@api_view(['POST', 'GET'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def stories(request):
    message = ""
    if request.method == 'POST':
        serializer = StorySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            message = "스토리 생성 성공"
        else: return HttpResponse(status=400)

    elif request.method == 'GET':
        stories = Story.objects.all()
        serializer = StorySerializer(stories, many=True)
        message = "모든 스토리 조회 성공"

    return JsonResponse({
        "message": message,
        "data": serializer.data
    }, safe=False)


@swagger_auto_schema(methods=['delete'], tags=['스토리'], operation_summary="스토리 삭제")
@swagger_auto_schema(methods=['put'], tags=['스토리'], operation_summary="스토리 수정", request_body=StorySerializer, operation_description=
    """
        - title : 이름
        - image : 대표이미지 (테스트 안됨)
        - difficulty : 난이도
        - color : 색상
    """)
@api_view(['DELETE', 'PUT'])
@permission_classes([IsAuthenticated])
@authentication_classes([JSONWebTokenAuthentication])
def modify_stories(request, story_id):
    if request.method == 'PUT':
        story = get_object_or_404(Story, id=story_id)
        serializer = StorySerializer(data=request.data, instance=story)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse({
                "message": "스토리 수정 성공",
                "data": serializer.data
            }, safe=False)

    elif request.method == 'DELETE':
        story = get_object_or_404(Story, id=story_id)
        story.delete()
        return JsonResponse({
            "message": "스토리 삭제 성공"
        })