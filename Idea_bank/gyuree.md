# 아이디어

## 2020.05.11

### 아이디어

아이들이 스토리에 능동적으로 참여를 할 수 있다면 좋을 것 같다.

주인공 캐릭터를 게임 시작 전에 선택을 할 수 있다. (남/여, 자신의 성별과는 관계없음)

주인공 캐릭터는 동화속을 탐험하며 동화속의 주인공을 도와주는 스토리로 진행한다.



### 스토리

스토리는 일단 동화 베이스로 진행한다.

결말은 동화와 다르게 진행되는데, 주인공의 도움으로 동화 속의 문제가 해결되는 컨셉을 잡았다.



**예시 - 백설공주**

사냥꾼이 총으로 백설공주를 잡으려고 한다.

주인공은 총을 그려 사냥꾼의 총을 뺏는다.

총을 잃어버린 사냥꾼은 돌아간다.

왕비가 사과바구니를 들고 등장한다.

주인공이 사과를 그리면, 왕비가 사과바구니를 떨어뜨리고 왕비는 돌아가게 된다.

하지만 굴러 떨어진 사과 하나를 백설공주가 우연히 먹게 되고 쓰러지게 된다.

백설공주를 깨우기 위해서 주인공은 그림을 그릴 수 있는데, 이 그림은 주제 여러개 중 하나를 그리거나, 자유로운 그림 하나를 그리게 해서 해결할 수 있다.

이렇게 해서 챕터를 깨면 백설공주 컬렉션과 그림 컬렉션을 얻을 수 있다.



### 요점

주인공을 넣어 동화 스토리를 조금 각색해서 풀어나가면 좋을 것 같다.



## 2020.05.12

### 스토리 모델 설정

```
Episode

id: 0 # 자동 생성
chapter: 0 # Foreign Key로 연결
script: [] # +버튼을 눌러서 캐릭터와 대사를 추가 (관리자 페이지)
pattern: "" # 초보자 난이도의 도안
```

Chapter와 User, Episode와 User는 Many to Many로 연결

Django에서 Episode를 Chapter로 필터링 후 User와 연결되어 있지 않은 가장 첫번째 Episode를 가져온다.

아니면 체크리스트 혹은 프로필을 만들어서 체크



### front 코드 구조

**화면 전환**

```vue
<template>
  <v-container>
    <div>
      {{ alice.title }} - {{ alice.subtitle }}
    </div>
    <div v-if="showcanvas === false">
      <div v-for="(episode, i) in alice.ep" :key="i">
        <div v-if="episode.id === epcount">
          <div v-for="(text, j) in episode.script" :key="j">
            <div v-if="j+1 === textcount">
              {{ text }}
            </div>
          </div>
          <v-btn v-if="maxtextcount !== textcount" @click="textnext">
            textnext 대사 넘기기
          </v-btn>
          <v-btn v-else-if="maxepcount >= epcount + 1" @click="drawingpage">
            그림 그리기
          </v-btn>
          <v-btn v-else>
            에피 종료
          </v-btn>
        </div>
      </div>
    </div>
    <!-- 그림 그리는 캔버스 페이지 -->
    <div v-else>
      캔버스
      <div v-if="answer === null">
        <v-btn @click="checkanswer">
          제출
        </v-btn>
      </div>
      <div v-else-if="answer === true">
        <v-btn v-if="maxepcount >= epcount" @click="epnext">
          epnext 그림 다 그리고 나서 보여줌
        </v-btn>
        <!-- <v-btn v-else>
          episode 종료 다른 화면 넘어가도록 이벤트 작성
        </v-btn> -->
      </div>
      <div v-else>
        <v-btn @click="backtodrawingpage">
          다시 풀어야함
        </v-btn>
      </div>
    </div>
  </v-container>
</template>

<script>
  import axios from "axios";

  export default {
    name: 'HelloWorld',

    data: () => ({
      alice: Object,
      epcount: 1,
      textcount: 1,
      maxepcount: 0,
      maxtextcount: 0,
      showcanvas: false,
      answer: null
    }),
    mounted() {
      this.test()
    },
    methods: {
      test() {
        const testUrl = "dummy/test.json"
        axios.get(testUrl)
        .then((res) => {
          console.log(res)
          this.alice = res.data
          this.maxepcount = res.data.epcount
          this.maxtextcount = res.data.ep[0].script.length
        })
        .catch((error) => {
          console.log(error)
        })
        console.log("test")
      },
      epnext() {
        this.epcount += 1
        this.textcount = 1
        this.maxtextcount = this.alice.ep[this.epcount-1].script.length
        this.showcanvas = false
      },
      textnext() {
        this.textcount += 1
      },
      drawingpage() {
        this.showcanvas = true
      },
      checkanswer() {
        // 여기서 axios 받아와야 함
        this.answer = true
      },
      backtodrawingpage() {
        this.answer = null
      }
    }
  }
</script>
```



**더미 데이터 (public에 위치)**

```json
{
    "title": "alice",
    "subtitle": "몰라",
    "epcount": 3,
    "ep": [
        {
            "id": 1,
            "script": [
                {"주인공": "ep1아무말1"},
                {"상대방1": "ep1아무말2"},
                {"주인공": "ep1아무말3"},
                {"상대방2": "ep1아무말4"},
                {"주인공": "ep1아무말5"}
            ],
            "pattern": ""
        },
        {
            "id": 2,
            "script": [
                {"주인공": "ep2아무말1"},
                {"상대방1": "ep2아무말2"},
                {"주인공": "ep2아무말3"},
                {"상대방2": "ep2아무말4"},
                {"주인공": "ep2아무말5"}
            ],
            "pattern": ""
        },
        {
            "id": 3,
            "script": [
                {"주인공": "epfinal아무말1"},
                {"상대방1": "epfinal아무말2"},
                {"주인공": "epfinal아무말3"},
                {"상대방2": "epfinal아무말4"},
                {"주인공": "epfinal아무말5"}
            ],
            "pattern": ""
        }
    ]
}
```



## 2020.05.13

### 페이지 구성

**메인페이지**

1. 로그인을 바로 할 수 있음
2. 회원가입(로그인을 안했을 경우만)
   - 이름, 나이, 성별, 캐릭터 선택
   - 모달창으로 띄워줌
3. 튜토리얼
4. 로그인 했을 경우 게임 시작 버튼 표시

**튜토리얼 페이지**

1. 게임에 대한 설명
2. 힌트 사용법 설명
3. 그림 그리는 방법 체험 (저장 x)

**게임 시작 페이지**

1. 스토리 모드
2. 마이페이지

**스토리 페이지**

1. 스토리 선택 버튼

**챕터 페이지**

1. 챕터 선택 버튼
2. 아직 플레이하지 않은 챕터는 잠겨있음

**게임 진행 페이지**

1. 스토리
   - 스크립트를 보여줌
   - 대사 별로 음성이 나오게 해줌
   - 스토리 스킵 기능
2. 그림
   - 그림 그리는 캔버스
   - 힌트를 누르면 키워드의 실루엣 사진을 보여줌
   - 제출하면 키워드와 맞는지 판별
   - 제출 3번 해서 틀릴경우 pass
   - 맞거나 틀릴 경우 모달창으로 알림

**마이페이지**

1. 유저 캐릭터 표시
2. 유저 캐릭터 관리 & 변경
3. 유저 컬렉션 보기
4. 유저 정보
5. 게임 플레이 시간 / 게임 횟수
6. 나의 원픽 그림 (액자 추가)

**컬렉션 페이지**

1. 챕터별로 구성
2. 그림을 시간순으로 보여줌
3. 그림 삭제 버튼
4. 그림 공유 기능 (카카오스토리 / 밴드)
5. 좋아요 기능 (나의 원픽)

**관리자 페이지**

1. 스토리
2. 챕터
3. 스크립트
4. 유저 관리
5. 메인페이지에서 로그인을 하면 관리자 전용 화면으로 넘어가도록



## 2020.05.14

### 목업

**메인 페이지**

![main1](assets/main1.png)

![main2](assets/main2.png)

![main3](assets/main3.png)



**튜토리얼 페이지**

![tutorial1](assets/tutorial1.png)

![tutorial2](assets/tutorial2.png)



**스토리 페이지**

![story1](assets/story1.png)



**챕터 페이지**

![chapter1](assets/chapter1.png)



**게임 진행 페이지**

![game1](assets/game1.png)

![game2](assets/game2.png)

![game3](assets/game3.png)



**마이 페이지**

![my1](assets/my1.png)

![my2](assets/my2.png)



**컬렉션 페이지**

![collection1](assets/collection1.png)