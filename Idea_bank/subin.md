# 2020.05.11.Mon

- 동화 정하기

  - 엉덩이 탐정

    > 엉덩이 탐정처럼 내가 주인공이고 그림을 그려가면서 사건을 해결해나가는 과정

  - 헨젤과 그레텔

    > 과자가 아닌 과일이나, 공 등

# 2020.05.14.Thu

- 프로그램 이름

  > 그려줘 친구들

- 미션 클리어 시 캐릭터

  - 머리 장식

    > 하트 여왕 챕터 클리어 시 하트 뿅뿅 머리띠 획득

  - 옷 장식

    > 앨리스 스토리 클리어 시 앨리스 옷 획득

  - 손 장식

    > 토끼 챕터 클리어 시 시계 획득

- 갤러리 페이지 목업

![gallery1](assets/gallery1.jpg)

![gallery2](assets/gallery2.jpg)

![gallery3](assets/gallery3.jpg)

# 2020.05.18.Mon

- 튜토리얼 페이지

  - 1페이지

  ![tutorial01](assets/tutorial01.png)

  - 2페이지

  ![tutorial02](assets/tutorial02.png)