import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import store from './store'
import router from './router'
import VueSignature from "vue-signature-pad"
import VueSimpleAlert from 'vue-simple-alert'
import VueTypedJs from 'vue-typed-js'

Vue.use(VueSignature)
Vue.use(VueSimpleAlert)
Vue.use(VueTypedJs)
Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  router,
  render: h => h(App)
}).$mount('#app')
