export default [
  {
    path: "",
    view: "Home",
    name: "home"
  },
  {
    path: "/story",
    view: "GameStory",
    name: "gamestory"
  },
  {
    path: "/story/:id/chapter",
    view: "GameChapter",
    name: "gamechapter"
  },
  {
    path: "/story/:id/chapter/:cid",
    view: "GamePlay",
    name: "gameplay"
  },
  {
    path: "/mypage",
    view: "MyPage",
    name: "mypage"
  },
  {
    path: "/gallery",
    view: "Gallery",
    name: "gallery"
  },
  {
    path: "/manage",
    view: "Admin",
    name: "admin"
  },
  {
    path: "/tutorial",
    view: "Tutorial",
    name: "tutorial"
  },
  {
    path: "/manage-story",
    view: "AdminStory",
    name: "adminstory",
  },
  {
    path: "/manage-user",
    view: "AdminUser",
    name: "adminuser"
  },
  {
    path: "/manage-sort",
    view: "AdminSort",
    name: "adminsort"
  },
  {
    path: "/manage-application",
    view: "AdminApplication",
    name: "adminapplication"
  },
  {
    path: "/manage-story/:id/chapter",
    view: "AdminChapter",
    name: "adminchapter"
  },
  {
    path: "/manage-story/:id/chapter/:cid",
    view: "AdminScriptPage",
    name: "adminscriptpage"
  },
  {
    path: "/manage-sort/:id/item",
    view: "AdminItem",
    name: "adminitem"
  },
  {
    path: "/manage-application/:id",
    view: "AdminAuthorApplication",
    name: "adminauthorapplication"
  },
  {
    path: "/about-us",
    view: "AboutUs",
    name: "aboutus"
  },
  {
    path: "/my-avatar",
    view: "Avatar",
    name: "avatar"
  },
  {
    path: "/apply-author",
    view: "ApplyAuthor",
    name: "applyauthor"
  },
]