import Vue from 'vue'
import Vuex from 'vuex'
import auth from "./modules/auth"
import play from "./modules/play"
import admin from "./modules/admin"
import player from "./modules/player"
import tutorial from "./modules/tutorial"
import gallery from "./modules/gallery"

import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

const plugins = [
  createPersistedState({
    storage: window.sessionStorage,
    paths: [
      "auth",
      "play",
      "admin",
      "player",
      "tutorial",
      "gallery",
    ]
  })
]

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    auth,
    play,
    admin,
    player,
    tutorial,
    gallery,
  },
  plugins: plugins,
})
