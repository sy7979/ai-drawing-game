const state = {
  story: null,
  storytitle: null,
  chapter: null,
  page: null,
}

const mutations = {
  setStory(state, story) {
    state.story = story
  },
  setStoryTitle(state, storytitle) {
    state.storytitle = storytitle
  },
  setChapter(state, chapter) {
    state.chapter = chapter
  },
  setPage(state, page) {
    state.page = page
  }
}

const actions = {
  getStory(options, story) {
    options.commit("setStory", story)
  },
  nullStory(options) {
    options.commit("setStory", null)
  },
  getStoryTitle(options, storytitle) {
    options.commit("setStoryTitle", storytitle)
  },
  nullStoryTitle(options) {
    options.commit("setStoryTitle", null)
  },
  getChapter(options, chapter) {
    options.commit("setChapter", chapter)
  },
  nullChapter(options) {
    options.commit("setChapter", null)
  },
  getPage(options, page) {
    options.commit("setPage", page)
  },
  nullPage(options) {
    options.commit("setPage", null)
  },
}

const getters = {
  storyId(state) {
    return state.story
  },
  storyTitle(state) {
    return state.storytitle
  },
  chapterId(state) {
    return state.chapter
  },
  pageId(state) {
    return state.page
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}