import jwtDecode from "jwt-decode"

const state = {
  token: null
}

const mutations = {
  setToken(state, token) {
    state.token = token
  },
}

const actions = {
  login(options, token) {
    options.commit("setToken", token)
  },
  logout(options) {
    options.commit("setToken", null)
  },
}

const getters = {
  isAuthenticated(state) {
    if (state.token) {
      return true
    } else {
      return false
    }
  },
  requestHeader(state) {
    return {
      headers: {
        Authorization: `JWT ${state.token}`
      }
    }
  },
  userId(state) {
    return state.token ? jwtDecode(state.token).user_id : null
  },
  userName(state) {
    return state.token ? jwtDecode(state.token).username : null
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}