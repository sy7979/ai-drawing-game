const state = {
  gallerystory: null,
  gallerychapter: null,
}

const mutations = {
  setGalleryStory(state, gallerystory) {
    state.gallerystory = gallerystory
  },
  setGalleryChapter(state, gallerychapter) {
    state.gallerychapter = gallerychapter
  },
}

const actions = {
  getGalleryStory(options, gallerystory) {
    options.commit("setGalleryStory", gallerystory)
  },
  nullGalleryStory(options) {
    options.commit("setGalleryStory", null)
  },
  getGalleryChapter(options, gallerychapter) {
    options.commit("setGalleryChapter", gallerychapter)
  },
  nullGalleryChapter(options) {
    options.commit("setGalleryChapter", null)
  },
}

const getters = {
  galleryStoryId(state) {
    return state.gallerystory
  },
  galleryChapterId(state) {
    return state.gallerychapter
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}