const state = {
  stay: null,
  gamestory: null,
  gamechapter: null,
  difficulty: null,
  missionstatus: null,
  answer: null,
  answerid: null,
  storyimage: null,
  gamebgm: null,
  straightnextchapter: false,
  nextchapterindex: null,
  nextchapterid: null,
  nextchapterbgm: null,
}

const mutations = {
  setStay(state, stay) {
    state.stay = stay
  },
  setGameStory(state, gamestory) {
    state.gamestory = gamestory
  },
  setGameChapter(state, gamechapter) {
    state.gamechapter = gamechapter
  },
  setGameStoryDifficulty(state, difficulty) {
    state.difficulty = difficulty
  },
  setMissionStatus(state, missionstatus) {
    state.missionstatus = missionstatus
  },
  setAnswer(state, answer) {
    state.answer = answer
  },
  setAnswerId(state, answerid) {
    state.answerid = answerid
  },
  setStoryImage(state, storyimage) {
    state.storyimage = storyimage
  },
  setGameBgm(state, gamebgm) {
    state.gamebgm = gamebgm
  },
  setStraightNextChapter(state, straightnextchapter) {
    state.straightnextchapter = straightnextchapter
  },
  setNextChapterIndex(state, nextchapterindex) {
    state.nextchapterindex = nextchapterindex
  },
  setNextChapterId(state, nextchapterid) {
    state.nextchapterid = nextchapterid
  },
  setNextChapterBgm(state, nextchapterbgm) {
    state.nextchapterbgm = nextchapterbgm
  },
}

const actions = {
  startStay(options, stay) {
    options.commit("setStay", stay)
  },
  endStay(options) {
    options.commit("setStay", null)
  },
  getGameStory(options, gamestory) {
    options.commit("setGameStory", gamestory)
  },
  nullGameStory(options) {
    options.commit("setGameStory", null)
  },
  getGameChapter(options, gamechapter) {
    options.commit("setGameChapter", gamechapter)
  },
  nullGameChapter(options) {
    options.commit("setGameChapter", null)
  },
  getGameStoryDifficulty(options, difficulty) {
    options.commit("setGameStoryDifficulty", difficulty)
  },
  nullGameStoryDifficulty(options) {
    options.commit("setGameStoryDifficulty", null)
  },
  getBeforeMissionStatus(options) {
    options.commit("setMissionStatus", false)
  },
  getAfterMissionStatus(options) {
    options.commit("setMissionStatus", true)
  },
  nullMissionStatus(options) {
    options.commit("setMissionStatus", null)
  },
  getAnswer(options, answer) {
    options.commit("setAnswer", answer)
  },
  nullAnswer(options) {
    options.commit("setAnswer", null)
  },
  getAnswerId(options, answerid) {
    options.commit("setAnswerId", answerid)
  },
  nullAnswerId(options) {
    options.commit("setAnswerId", null)
  },
  getStoryImage(options, storyimage) {
    options.commit("setStoryImage", storyimage)
  },
  nullStoryImage(options) {
    options.commit("setStoryImage", null)
  },
  getGameBgm(options, gamebgm) {
    options.commit("setGameBgm", gamebgm)
  },
  nullGameBgm(options) {
    options.commit("setGameBgm", null)
  },
  goStraightNextChapter(options) {
    options.commit("setStraightNextChapter", true)
  },
  cancelStraightNextChapter(options) {
    options.commit("setStraightNextChapter", false)
  },
  getNextChapterIndex(options, nextchapterindex) {
    options.commit("setNextChapterIndex", nextchapterindex)
  },
  nullNextChapterIndex(options) {
    options.commit("setNextChapterIndex", null)
  },
  getNextChapterId(options, nextchapterid) {
    options.commit("setNextChapterId", nextchapterid)
  },
  nullNextChapterId(options) {
    options.commit("setNextChapterId", null)
  },
  getNextChapterBgm(options, nextchapterbgm) {
    options.commit("setNextChapterBgm", nextchapterbgm)
  },
  nullNextChapterBgm(options) {
    options.commit("setNextChapterBgm", null)
  },
}

const getters = {
  isStartStay(state) {
    if (state.stay) {
      return true
    } else {
      return false
    }
  },
  stayId(state) {
    return state.stay
  },
  gameStoryId(state) {
    return state.gamestory
  },
  gameChapterId(state) {
    return state.gamechapter
  },
  gameStoryDifficulty(state) {
    return state.difficulty
  },
  gameMissionStatus(state) {
    return state.missionstatus
  },
  getAnswer(state) {
    return state.answer
  },
  answerId(state) {
    return state.answerid
  },
  gameStoryImageSrc(state) {
    return state.storyimage
  },
  gameBgm(state) {
    return state.gamebgm
  },
  straightNextChapter(state) {
    return state.straightnextchapter
  },
  nextChapterIndex(state) {
    return state.nextchapterindex
  },
  nextChapterId(state) {
    return state.nextchapterid
  },
  nextChapterBgm(state) {
    return state.nextchapterbgm
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}