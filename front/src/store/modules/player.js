const state = {
  staff: null,
  playername: null,
  teampage: null,
}

const mutations = {
  setStaff(state, staff) {
    state.staff = staff
  },
  setPlayerName(state, playername) {
    state.playername = playername
  },
  setTeamPage(state, teampage) {
    state.teampage = teampage
  },
}

const actions = {
  getStaff(options, staff) {
    options.commit("setStaff", staff)
  },
  nullStaff(options) {
    options.commit("setStaff", null)
  },
  getPlayerName(options, playername) {
    options.commit("setPlayerName", playername)
  },
  nullPlayerName(options) {
    options.commit("setPlayerName", null)
  },
  getTeamPage(options) {
    options.commit("setTeamPage", true)
  },
  nullTeamPage(options) {
    options.commit("setTeamPage", null)
  },
}

const getters = {
  isStaff(state) {
    return state.staff
  },
  playerName(state) {
    return state.playername
  },
  isTeamPage(state) {
    return state.teampage
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}