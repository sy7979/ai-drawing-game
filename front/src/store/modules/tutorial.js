const state = {
  tutorial: null,
}

const mutations = {
  setTutorial(state, tutorial) {
    state.tutorial = tutorial
  },
}

const actions = {
  getTutorial(options, tutorial) {
    options.commit("setTutorial", tutorial)
  },
  nullTutorial(options) {
    options.commit("setTutorial", null)
  },
}

const getters = {
  isTutorial(state) {
    return state.tutorial
  },
}

export default {
  state,
  mutations,
  actions,
  getters,
}